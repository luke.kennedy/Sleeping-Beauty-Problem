package main

import (
	"crypto/rand"
	"log"

	"github.com/spf13/cobra"
)

func coinIsHeads() bool {
	out := []byte{0}
	_, err := rand.Read(out)
	if err != nil {
		panic(err)
	}
	return out[0] >= 128
}

type guess struct {
	day   int16
	heads bool
}

func main() {
	var (
		numWakings *int16
		numTrials  *int32
	)
	cmd := cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			// outcomes := make([]guess, 0, *numTrials)
			correctGuesses := int64(0)
			incorrectGuesses := int64(0)
			wakings := *numWakings
			trials := *numTrials
			for i := int32(0); i < trials; i++ {
				if coinIsHeads() {
					correctGuesses++
				} else {
					incorrectGuesses += int64(wakings)
				}
			}
			log.Println(correctGuesses, incorrectGuesses)
			return nil
		},
	}
	numWakings = cmd.Flags().Int16P("wakings", "w", int16(1000), "")
	numTrials = cmd.Flags().Int32P("trials", "t", int32(1000000), "")
	cmd.Execute()
}
